(TeX-add-style-hook "cv_9"
 (lambda ()
    (LaTeX-add-labels
     "sec:career-object"
     "sec:course-projects")
    (TeX-add-symbols
     "fb")
    (TeX-run-style-hooks
     "titlesec"
     "hyperref"
     "layaureo"
     "big"
     "xcolor"
     "dvipsnames"
     "usenames"
     "parskip"
     "url"
     "xltxtra"
     "xunicode"
     "fontspec"
     ""
     "latex2e"
     "art10"
     "article"
     "10pt"
     "a4paper")))

